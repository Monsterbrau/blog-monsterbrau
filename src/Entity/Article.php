<?php

namespace App\Entity;

class Article
{
  public $id;
  public $title;
  public $secondTitle;
  public $date;
  public $content;
  public $resume;
  public $cat;

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->secondTitle = $sql["secondTitle"];
    $this->date = $sql["date"];
    $this->content = $sql["content"];
    $this->resume = $sql["resume"];
    $this->cat = $sql["cat"];
  }
}