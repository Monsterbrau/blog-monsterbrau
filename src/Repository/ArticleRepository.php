<?php

namespace App\Repository;

use App\Entity\Article;
use App\Util\Connexion;


class ArticleRepository
{
  public function getAll() : array
  {
    $articles = [];
    try {
      $cnx = Connexion::getConnexion();

      $query = $cnx->prepare("SELECT * FROM Article ORDER BY id DESC");

      $query->execute();

      foreach ($query->fetchAll() as $row) {
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

    } catch (\PDOException $e) {
      dump($e);
    }
    return $articles;
  }

  public function add(Article $article)
  {
    try {
      $cnx = Connexion::getConnexion();

      $query = $cnx->prepare("INSERT INTO Article (title,secondTitle,date,resume,content,cat) VALUES (:title,:secondTitle,:date,:resume,:content,:cat)");

      $query->bindValue(":title", $article->title);
      $query->bindValue(":secondTitle", $article->secondTitle);
      $query->bindValue(":date", $article->date);
      $query->bindValue(":resume", $article->resume);
      $query->bindValue(":content", $article->content);
      $query->bindValue(":cat", $article->cat);

      $query->execute();

      $article->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
      dump($e);
    };
  }
  public function update(Article $article)
  {
    try {
      $cnx = Connexion::getConnexion();

      $query = $cnx->prepare("UPDATE Article SET title=:title,secondTitle=:secondTitle,date=:date,resume= :resume,content=:content,cat=:cat WHERE id=:id");

      $query->bindValue(":title", $article->title);
      $query->bindValue(":secondTitle", $article->secondTitle);
      $query->bindValue(":date", $article->date);
      $query->bindValue(":resume", $article->resume);
      $query->bindValue(":content", $article->content);
      $query->bindValue(":cat", $article->cat);
      $query->bindValue(":id", $article->id);
      $query->execute();

    } catch (\PDOException $e) {
      dump($e);
    };
  }

  public function getById(int $id) : ? Article
  {
    try {
      $cnx = Connexion::getConnexion();

      $query = $cnx->prepare("SELECT * FROM Article WHERE id=:id");

      $query->bindValue(":id", $id);

      $query->execute();

      $result = $query->fetchAll();

      if (count($result) === 1) {
        $article = new Article();
        $article->fromSQL($result[0]);
        return $article;
      }

    } catch (\PDOException $e) {
      dump($e);
    };
    return null;
  }

  public function delete(int $id)
  {
    try {
      $cnx = Connexion::getConnexion();

      $query = $cnx->prepare("DELETE FROM Article WHERE id=:id");

      $query->bindValue(":id", $id);

      return $query->execute();

    } catch (\PDOException $e) {
      dump($e);
    }
    return false;
  }
}

