<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\ArticleAdminType;

class BlogArticleUserController extends AbstractController
{

  /**
   * @Route ("/articleUser/{id}",name="articleUser")
   */

  public function index(int $id, ArticleRepository $repo)
  {
    $article = $repo->getById($id);

    return $this->render('articleUser.html.twig', [
      'article' => $article,]);
  }
}