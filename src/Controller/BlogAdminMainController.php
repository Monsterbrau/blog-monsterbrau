<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
* @Security("has_role('ROLE_ADMIN')")
*/

class BlogAdminMainController extends AbstractController
{

  /**
   * @Route ("/adminMain",name="adminMain")
   */

  public function index(ArticleRepository $repo)
  {
    $result = $repo->getAll();

    return $this->render("adminMain.html.twig", [
      'result' => $result,
    ]);
  }
}