<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Form\ArticleAdminType;
use Symfony\Component\HttpFoundation\Request;

class UpdateController extends AbstractController
{

  /**
   * @Route ("/update/{id}",name="update")
   */

  public function index(int $id,ArticleRepository $repo,Request $request)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleAdminType::class,$article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            
            $repo->update($form->getData());

            return $this->redirectToRoute("adminMain");
        
        }
        return $this->render('Update.html.twig', [
            'controller_name' => 'UpdateController',
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }
}