<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class BlogMainController extends AbstractController
{

  /**
   * @Route ("/",name="userMain")
   */

  public function index(ArticleRepository $repo)
  {
    $result = $repo->getAll();

    return $this->render("userMain.html.twig",[
      'result' => $result,
    ]);
  }
}