<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Symfony\Component\Form\FormTypeInterface;
use App\Form\ArticleAdminType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;



class AddArticleController extends Controller
{
    /**
     * @Route("/addArticle", name="addArticle")
     */

    public function index(ArticleRepository $repo, Request $req)
    {
        $article = new Article();

        $form = $this->createForm(ArticleAdminType::class, $article);

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();
            $repo->add($article);
            return $this->redirectToRoute("adminMain");

        }

        return $this->render('addArticle.html.twig', [
            'AddArticleController' => 'AddArticleController',
            'form' => $form->createView(),
            'article' => $article
        ]);
    }
}
