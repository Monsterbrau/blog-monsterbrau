<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class DeleteArticleController extends AbstractController
{

  /**
   * @Route ("/delete/{id}",name="delete")
   */

  public function index(int $id, ArticleRepository $repo)
  {
    $article = $repo->delete($id);

    return $this->redirectToRoute("adminMain");
  }
}