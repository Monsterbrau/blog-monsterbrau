<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Admin;

class BlogConnexionController extends AbstractController
{

  /**
   * @Route ("/connexion",name="connexion")
   */

  public function index()
  {return $this->render("connexion.html.twig",[
      ]);
  }
}