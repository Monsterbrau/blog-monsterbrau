<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

class ArticleAdminController extends AbstractController
{

  /**
   * @Route ("/adminArticle/{id}",name="adminArticle")
   */

  public function index(int $id, ArticleRepository $repo)
  {
    $article = $repo->getById($id);

    return $this->render('articleAdmin.html.twig', [
      'article' => $article,]);
  }
}