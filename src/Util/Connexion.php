<?php

namespace App\Util;

class Connexion
{
  public static function getConnexion() : \PDO
  {
    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

    $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    return $cnx;
  }
}